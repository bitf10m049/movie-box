class CreateBoxedMovies < ActiveRecord::Migration
  def change
    create_table :boxed_movies do |t|
      t.references :box, index: true, foreign_key: true
      t.references :movie, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
