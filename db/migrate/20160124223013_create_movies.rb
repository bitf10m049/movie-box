class CreateMovies < ActiveRecord::Migration
  def change
    create_table :movies do |t|
      t.string :title
      t.text :story_line
      t.string :director_name
      t.string :year
      t.datetime :release_date

      t.timestamps null: false
    end
  end
end
