# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

admin = User.create(first_name: Faker::Name.first_name, last_name: Faker::Name.last_name, role: 'admin')

user_list = [*1..10].map do
              User.create(first_name: Faker::Name.first_name, last_name: Faker::Name.last_name, role: 'user')
            end

movies_list = [*1..10].map do
                Movie.create(title: Faker::Book.title, story_line: Faker::Lorem.paragraph, director_name: Faker::Name.first_name,
                               release_date: Faker::Date.birthday(1,29) )
              end

user_list.each do |user|
  title = ['hehehe','horror','favourite', 'action', 'best for travel'].sample
  box = user.boxes.create(title: title)
  box.movies << movies_list.first([*1..10].sample)
end


