class MovieSerializer < ActiveModel::Serializer
  attributes :id, :title, :story_line, :director_name, :year, :release_date
end
