# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  first_name :string
#  last_name  :string
#  role       :integer
#  api_key    :string
#  email      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class User < ActiveRecord::Base
  validates :first_name, :last_name, :role,presence: true
  validates :api_key, uniqueness: true

  before_create :assign_api_key

  enum role: [:admin, :user]

  has_many :boxes

  def assign_api_key
    self.api_key = generate_api_key
  end

  def generate_api_key
    loop do
      token = SecureRandom.urlsafe_base64
      break token unless User.exists?(api_key: token)
    end
  end
end
