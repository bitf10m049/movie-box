class Movie < ActiveRecord::Base
  validates :title, presence: true

  has_many :boxed_movies
  has_many :boxes, through: :boxed_movies
end
