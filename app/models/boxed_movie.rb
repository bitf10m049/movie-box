class BoxedMovie < ActiveRecord::Base
  belongs_to :box
  belongs_to :movie
end
