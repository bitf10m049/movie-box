class Box < ActiveRecord::Base
  validates  :title, presence: true

  belongs_to :user
  has_many   :boxed_movies
  has_many   :movies, through: :boxed_movies
end
