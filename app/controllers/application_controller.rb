class ApplicationController < ActionController::API
  include CanCan::ControllerAdditions
  include ActionController::Serialization
  include Authenticable
  before_filter :authenticate_with_token!

  rescue_from CanCan::AccessDenied do |exception|
    render json: {errors: exception.message}.to_json, status: 403
  end

  rescue_from ActiveRecord::RecordNotFound do |e|
    render json: {errors: e.message}.to_json, status: 404
  end
end
