class Api::V1::UsersController < ApplicationController
  # before_action :set_user, only: [:show, :update, :destroy]
  load_and_authorize_resource

  # GET /users
  # GET /users.json
  def index
    if @current_user.admin?
      render json: @users
    else
      render json: @users, except: [:api_key]
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    if @current_user.admin? or @current_user == @user
      render json: @user
    else
      render json: @user, except: [:api_key]
    end
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    if @user.save
      render json: @user, status: :created, location: api_v1_user_path(@user)
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    if @user.update(user_params)
      head :no_content
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy

    head :no_content
  end

  private

    # def set_user
    #   @user = User.find(params[:id])
    # end

    def user_params
      params.require(:user).permit(:first_name, :last_name, :role, :api_key, :email)
    end
end
