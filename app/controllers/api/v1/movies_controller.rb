class Api::V1::MoviesController < ApplicationController
  # before_action :set_movie, only: [:show, :update, :destroy]
  load_and_authorize_resource

  # GET /movies
  # GET /movies.json
  def index
    render json: @movies
  end

  # GET /movies/1
  # GET /movies/1.json
  def show
    render json: @movie
  end

  # POST /movies
  # POST /movies.json
  def create
    @movie = Movie.new(movie_params)

    if @movie.save
      render json: @movie, status: :created
    else
      render json: @movie.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /movies/1
  # PATCH/PUT /movies/1.json
  def update
    if @movie.update(movie_params)
      head :no_content
    else
      render json: @movie.errors, status: :unprocessable_entity
    end
  end

  # DELETE /movies/1
  # DELETE /movies/1.json
  def destroy
    @movie.destroy

    head :no_content
  end

  private

    # def set_movie
    #   @movie = Movie.find(params[:id])
    # end

    def movie_params
      params.require(:movie).permit(:title, :story_line, :director_name, :year, :release_date)
    end
end
