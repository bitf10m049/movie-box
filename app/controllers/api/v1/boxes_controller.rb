class Api::V1::BoxesController < ApplicationController
  # before_action :set_box, only: [:show, :update, :destroy]
  load_and_authorize_resource :user
  load_and_authorize_resource through: :user

  # GET /boxes
  # GET /boxes.json
  def index
    render json: @boxes
  end

  # GET /boxes/1
  # GET /boxes/1.json
  def show
    render json: @box
  end

  # POST /boxes
  # POST /boxes.json
  def create
    if @box.save
      render json: @box, status: :created
    else
      render json: @box.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /boxes/1
  # PATCH/PUT /boxes/1.json
  def update
    if @box.update(box_params)
      head :no_content
    else
      render json: @box.errors, status: :unprocessable_entity
    end
  end

  def add_movie
    @movie = Movie.find(params[:movie_id])
    if @movie.present?
      @box.movies << @movie
      render json: {success: "Movie with title '#{@movie.title}' added to box"}.to_json, status: :created
    end
  end

  def show_movies
    @movies = @box.movies
    render json: @movies
  end
  # DELETE /boxes/1
  # DELETE /boxes/1.json
  def destroy
    @box.destroy
    head :no_content
  end

  private

    # def set_box
    #   @box = Box.find(params[:id])
    # end

    def box_params
      params.require(:box).permit(:title)
    end
end
