module Authenticable
  def current_user
    @current_user ||= if request.headers[:authorization].present?
                        User.find_by_api_key(request.headers[:authorization])
                      else
                        User.new
                      end
  end

  def authenticate_with_token!
    render json: { errors: "Not authenticated" }, status: :unauthorized unless current_user.present?
  end
end