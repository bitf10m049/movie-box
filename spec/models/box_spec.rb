require 'rails_helper'

describe Box, 'validation' do
  it { should validate_presence_of :title }
end

describe Box, 'association' do
  it { should belong_to(:user) }
  it { should have_many(:boxed_movies) }
  it { should have_many(:movies).through(:boxed_movies) }
end