require 'rails_helper'

describe BoxedMovie, 'association' do
  it { should belong_to(:movie) }
  it { should belong_to(:box)   }
end
