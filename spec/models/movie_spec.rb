require 'rails_helper'

describe Movie, 'validations' do
  it { should validate_presence_of :title }
end

describe Movie, 'association' do
  it { should have_many :boxed_movies }
  it { should have_many :boxes }
end