# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  first_name :string
#  last_name  :string
#  role       :integer
#  api_key    :string
#  email      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

describe User, 'validation' do
  subject { create(:user)}
  it { should validate_presence_of(:first_name) }
  it { should validate_presence_of(:last_name) }
  it { should validate_presence_of(:role) }
  it { should validate_uniqueness_of(:api_key).ignoring_case_sensitivity }
end

describe User, 'association' do
  it { should have_many(:boxes) }
end

describe User, 'functionality' do
  it { should define_enum_for(:role).with([:admin, :user])}
  it { should respond_to(:generate_api_key)}
  it { should respond_to(:assign_api_key)}

  it 'creates unique token when saving' do
    expect(create(:user).api_key).not_to eq(create(:user).api_key)
  end
end



