require 'rails_helper'
require 'cancan/matchers'

describe 'Ability' do

  describe 'as guest user' do
    before(:each) do
      @ability = Ability.new(nil)
    end

    it 'can only view users' do
      expect(@ability).to be_able_to(:show, :users)
    end

    it 'can only view movies' do
      expect(@ability).to be_able_to(:show, :movies)
      expect(@ability).not_to be_able_to(:create, :movies)
      expect(@ability).not_to be_able_to(:update, :movies)
      expect(@ability).not_to be_able_to(:destroy, :movies)
    end

    it 'can only view others movie boxes' do
      expect(@ability).to be_able_to(:show, :boxes)
      expect(@ability).not_to be_able_to(:create, :boxes)
      expect(@ability).not_to be_able_to(:update, :boxes)
      expect(@ability).not_to be_able_to(:destroy, :boxes)
    end
  end

  describe 'as normal user' do
    before :each do
      @user       = create(:user)
      @other_user = create(:user, api_key:'my key')
      @ability    = Ability.new(@user)
    end

    it 'can update own information' do
      expect(@ability).to be_able_to(:update, @user)
    end

    it 'can not update others information' do
      expect(@ability).not_to be_able_to(:update, @other_user)
    end

    it 'can only delete it self' do
      expect(@ability).to be_able_to(:destroy, @user)
    end

    it 'can not delete other users' do
      expect(@ability).not_to be_able_to(:destroy, @other_user)
    end

    it 'can manage only own movie box' do
      expect(@ability).to be_able_to(:manage, Box)
    end

    it 'can not manage someone else movie box' do
      other_movie_box = create(:box, title:'Movie Box', user: @other_user)
      expect(@ability).not_to be_able_to(:manage, other_movie_box)
    end

    it 'can not create movies' do
      expect(@ability).not_to be_able_to(:mange, Movie)
    end
  end

  describe 'as admin user' do
    before :each do
      @user    = create(:admin)
      @ability = Ability.new(@user)
    end

    it 'can manage all' do
      expect(@ability).to be_able_to(:manage, :all)
    end
  end

end