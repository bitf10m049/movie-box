# Added Temporary Authentication class to test Authenticable module
class Authentication
  include Authenticable
end

describe Authenticable do
  let(:authentication){ Authentication.new }
  subject { authentication }

  describe '#current_user' do
    before do
      @user = create(:user)
      request.headers['Authorization'] = @user.api_key
      authentication.stub(:request).and_return(request)
    end

    it 'returns the user from the authorization header' do
      expect(authentication.current_user.api_key).to eql @user.api_key
    end

    context 'When authorization header is missing' do
      before do
        request.headers['Authorization'] = nil
        authentication.stub(:request).and_return(request)
      end

      it 'returns new user (as guest)' do
        expect(authentication.current_user.id).to be_nil
      end
    end
  end

  describe '#authenticate_with_token' do
    before do
      @user = create(:user)
      authentication.stub(:current_user).and_return(nil)
      response.stub(:response_code).and_return(401)
      response.stub(:body).and_return({errors: 'Not authenticated'}.to_json)
      authentication.stub(:response).and_return(response)
    end

    it 'render a json error message' do
      expect(json_response[:errors]).to eq('Not authenticated')
    end

    it { should respond_with 401 }
  end
end