require 'rails_helper'
describe Api::V1::MoviesController, :type => :controller do

  let(:valid_attributes)   { attributes_for(:movie)}
  let(:invalid_attributes) { {blabla: 'bla bla'} }

  describe 'GET index' do
    it 'assigns all movies as @movies' do
      movie = Movie.create! valid_attributes
      get :index, {}
      expect(assigns(:movies)).to eq([movie])
    end

    it { expect(response.status).to eq(200) }
  end

  describe 'GET show' do
    it 'assigns the requested movie as @movie' do
      movie = Movie.create! valid_attributes
      get :show, {:id => movie.to_param}
      expect(assigns(:movie)).to eq(movie)
    end

    it { expect(response.status).to eq(200) }
  end

  describe 'POST create' do
    describe 'with valid params' do
      it 'creates a new Movie' do
        request_from(:admin)
        expect {
          post :create, {:movie => valid_attributes}
        }.to change(Movie, :count).by(1)
      end

      it 'assigns a newly created movie as @movie' do
        request_from(:admin)
        post :create, {:movie => valid_attributes}
        expect(assigns(:movie)).to be_a(Movie)
        expect(assigns(:movie)).to be_persisted
      end

      context 'when user is admin' do
        it 'can create movie' do
          request_from(:admin)
          post :create, {:movie => valid_attributes}
          expect(response.status).to eq(201)
        end
      end

      context 'when user is not admin' do
        it 'can not create movie' do
          request_from(:user)
          post :create, {:movie => valid_attributes}
          expect(response.status).to eq(403)
        end
      end
    end

    describe 'with invalid params' do
      it 'assigns a newly created but unsaved movie as @movie' do
        post :create, {:movie => invalid_attributes}
        expect(assigns(:movie)).to be_a_new(Movie)
      end
    end
  end

  describe 'PUT update' do
    describe 'with valid params' do
      let(:new_attributes) {{title:'My Movie', story_line: 'In This movie...', director_name: 'MyString'}}
      it 'updates the requested movie' do
        request_from(:admin)
        movie = Movie.create! valid_attributes
        put :update, {:id => movie.to_param, :movie => new_attributes}
        movie.reload
        expect(movie.title).to eq(new_attributes[:title])
      end

      it 'assigns the requested movie as @movie' do
        request_from(:admin)
        movie = Movie.create! valid_attributes
        put :update, {:id => movie.to_param, :movie => valid_attributes}
        expect(assigns(:movie)).to eq(movie)
      end

      context 'when user is admin' do
        it 'can update movie' do
          request_from(:admin)
          movie = Movie.create! valid_attributes
          put :update, {:id => movie.to_param, :movie => new_attributes}
          expect(response.status).to eq(204)
        end
      end

      context 'when user is not admin' do
        it 'can not update movie' do
          request_from(:user)
          movie = Movie.create! valid_attributes
          put :update, {:id => movie.to_param, :movie => new_attributes}
          expect(response.status).to eq(403)
        end
      end
    end

    describe 'with invalid params' do
      it 'assigns the movie as @movie' do
        movie = Movie.create! valid_attributes
        put :update, {:id => movie.to_param, :movie => invalid_attributes}
        expect(assigns(:movie)).to eq(movie)
      end
    end
  end

  describe 'DELETE destroy' do
    it 'destroys the requested movie' do
      request_from(:admin)
      movie = Movie.create! valid_attributes
      expect {
        delete :destroy, {:id => movie.to_param}
      }.to change(Movie, :count).by(-1)
    end
  end
end
