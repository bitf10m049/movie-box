describe Api::V1::UsersController, :type => :controller do

  let(:valid_attributes) { attributes_for(:user) }
  let(:invalid_attributes) { {blabla: 'asdfasfasd'} }
  let(:valid_session) { {} }

  before do
    @access_error_message = CanCan::AccessDenied.new.to_s
  end
  
  describe 'GET index' do
    it 'assigns all users as @users' do
      user = User.create! valid_attributes
      get :index
      expect(assigns(:users)).to eq([user])
    end

    context 'when user is admin' do
      before do
        request_from(:admin)
        get :index
      end

      it 'get all information of each user' do
        expect(json_response[:users].first.keys).to include(:api_key)
      end
    end

    context 'when user is normal user' do
      before do
        request_from(:user)
        get :index
      end

      it 'should not see api_keys of users' do
        expect(json_response[:users].first.keys).not_to include(:api_key)
      end
    end
  end

  describe 'GET show' do
    it 'assigns the requested user as @user' do
      @created_user = User.create! valid_attributes
      get :show, {:id => @created_user.to_param}
      expect(assigns(:user)).to eq(@created_user)
    end

    context 'when user is admin it can see secrets of other users' do
      before do
        request_from(:admin)
        get :show, {id: create(:user).to_param}
      end
      it { expect(json_response[:user].keys).to include(:api_key)}
    end

    context 'when user is normal user' do
      before do
        request_from(:user)
      end

      it 'can see its own secrets' do
        get :show, {id: @user.to_param}
        expect(json_response[:user].keys).to include(:api_key)
      end

      it 'can not see secrets of others' do
        get :show, {id: create(:user).to_param}
        expect(json_response[:user].keys).not_to include(:api_key)
      end
    end
  end


  describe 'POST create' do
    context 'when request from admin user' do
      before do
        request_from(:admin)
      end
      describe 'with valid params' do
        it 'creates a new User' do
          expect {
            post :create, {:user => valid_attributes}
          }.to change(User, :count).by(1)
        end

        it 'assigns a newly created user as @user' do
          post :create, {:user => valid_attributes}
          expect(assigns(:user)).to be_a(User)
          expect(assigns(:user)).to be_persisted
        end

        it { expect respond_with 200 }
      end
      describe 'with invalid params' do
        it 'assigns a newly created but unsaved user as @user' do
          post :create, {:user => invalid_attributes}
          expect(assigns(:user)).to be_a_new(User)
        end

        it { expect(respond_with(422)) }
      end
    end

    context 'when user is not admin' do
      before do
        request_from(:user)
        post :create, {:user => valid_attributes}
      end
      it { expect(json_response[:errors]).to eq(@access_error_message)}
      it { expect(respond_with(403)) }
    end
  end

  describe 'PUT update' do
    describe 'with valid params' do
      let(:new_attributes) { {first_name: Faker::Name.first_name, last_name: Faker::Name.last_name} }

      it 'updates the requested user' do
        user = User.create! valid_attributes
        put :update, {:id => user.to_param, :user => new_attributes}
        user.reload
        expect(user[:first_name])
      end

      it 'assigns the requested user as @user' do
        user = User.create! valid_attributes
        put :update, {:id => user.to_param, :user => valid_attributes}, valid_session
        expect(assigns(:user)).to eq(user)
      end
    end

    describe 'with invalid params' do
      it 'assigns the user as @user' do
        user = User.create! valid_attributes
        put :update, {:id => user.to_param, :user => invalid_attributes}, valid_session
        expect(assigns(:user)).to eq(user)
      end
    end
  end

  describe 'DELETE destroy' do
    it 'destroys the requested user' do
      request_from(:user)
      expect {
        delete :destroy, {:id => @user.to_param}
      }.to change(User, :count).by(-1)
    end
  end
end





