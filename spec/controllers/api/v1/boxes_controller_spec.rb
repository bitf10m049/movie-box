require 'rails_helper'

describe Api::V1::BoxesController, :type => :controller do

  let(:valid_attributes)   { attributes_for :box }
  let(:invalid_attributes) { {blabla: 'blabla'} }

  before(:each) do
    @user  = create(:user)
    @admin = create(:admin)
  end

  describe 'GET index' do
    it 'assigns all boxes as @boxes' do
      box = Box.create! valid_attributes.merge(user: @user)
      get :index, user_id: @user.id
      expect(assigns(:boxes)).to eq([box])
    end
  end

  describe 'GET show' do
    it 'assigns the requested box as @box' do
      box = Box.create! valid_attributes.merge(user: @user)
      get :show, { id: box.to_param, user_id: @user.id }
      expect(assigns(:box)).to eq(box)
    end
  end

  describe 'POST create' do
    describe 'with valid params' do
      it 'creates a new Box' do
        api_authorization_header @user.api_key
        expect {
          post :create, {:box => valid_attributes, user_id: @user}
        }.to change(Box, :count).by(1)
      end

      it 'assigns a newly created box as @box' do
        api_authorization_header @user.api_key
        post :create, {:box => valid_attributes, user_id: @user}
        expect(assigns(:box)).to be_a(Box)
        expect(assigns(:box)).to be_persisted
      end

      describe 'current user' do
        it 'cannot create box for other users' do
          api_authorization_header @user.api_key
          other_user = create(:user)
          post :create, {:box => valid_attributes, user_id: other_user.id}
          expect(response).to have_http_status(403)
        end
      end
    end

    describe 'with invalid params' do
      it 'assigns a newly created but unsaved box as @box' do
        post :create, {:box => invalid_attributes, user_id: @user}
        expect(assigns(:box)).to be_a_new(Box)
      end
    end
  end

  describe 'PUT update' do
    describe 'with valid params' do
      let(:new_attributes) { { title: 'New Title' } }
      before do
        api_authorization_header @user.api_key
      end

      it 'updates the requested box' do
        box = Box.create! valid_attributes.merge(user: @user)
        put :update, {:id => box.to_param, :box => new_attributes, user_id: @user}
        box.reload
        expect(box.title).to eq(new_attributes[:title])
      end

      it 'assigns the requested box as @box' do
        box = Box.create! valid_attributes.merge(user: @user)
        put :update, {:id => box.to_param, :box => valid_attributes, user_id: @user}
        expect(assigns(:box)).to eq(box)
      end
    end

    describe 'with invalid params' do
      it 'assigns the box as @box' do
        box = Box.create! valid_attributes.merge(user: @user)
        put :update, {:id => box.to_param, :box => invalid_attributes, user_id: @user}
        expect(assigns(:box)).to eq(box)
      end
    end
  end

  describe 'POST movies' do
    let(:new_movie){ create(:movie) }

    it 'adds movie to box' do
      api_authorization_header @user.api_key 
      box = Box.create! valid_attributes.merge(user: @user)
      expect{
        post :add_movie, {id: box.to_param, movie_id: new_movie.id, user_id: @user}
      }.to change(box.movies, :count).by(1)
    end

    context "when movie dosen't exist" do
      before do
        api_authorization_header @user.api_key
        @box = Box.create! valid_attributes.merge(user: @user)
        post :add_movie, {id: @box.to_param, movie_id: 100000000, user_id: @user}
      end

      it 'cannot add movie that dosent exist' do
        expect{response}.to change(@box.movies, :count).by(0)
      end

      it { expect(response).to have_http_status(404) }
    end

    describe 'current user' do
      it "can not add movie to other user's box" do
        api_authorization_header @user.api_key
        other_user = create(:user)
        box        = Box.create! valid_attributes.merge(user: other_user)
        post :add_movie, {id: box.to_param, movie_id: new_movie.id, user_id: other_user}
        expect(response).to have_http_status(403)
      end
    end
  end

  describe 'GET movies' do
    it 'assigns all movies to @movies' do
      api_authorization_header @user.api_key
      box   = Box.create(title:'my box', user: @user)
      movie = Movie.create(title: 'Temp Movie')
      box.movies << movie
      get  :show_movies, { id: box.id, user_id: @user.id }
      expect(assigns(:movies)).to eq([movie])
    end
  end

  describe 'DELETE destroy' do
    it 'destroys the requested box' do
      request_from(:user)
      box = Box.create! valid_attributes.merge(user: @user)
      expect {
        delete :destroy, {:id => box.to_param, user_id: @user}
      }.to change(Box, :count).by(-1)
    end

  end
end
