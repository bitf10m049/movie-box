FactoryGirl.define do
  factory :movie do
    title      "MyString"
    story_line "MyText"
    director_name "MyString"
    year "MyString"
    release_date "2016-01-25 03:30:13"
  end
end
