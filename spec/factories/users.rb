# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  first_name :string
#  last_name  :string
#  role       :integer
#  api_key    :string
#  email      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :admin, class: User do
    first_name Faker::Name.first_name 
    last_name  Faker::Name.last_name
    role       'admin'
    api_key    nil
    email      "MyString"
  end

  factory :user do
    first_name Faker::Name.first_name
    last_name  Faker::Name.last_name
    role       'user'
    api_key    nil
    email      "MyString"
  end

end
