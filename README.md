## Movie Box 

Movie box is an application for user who are very possessive about their all time favourite Movies and want to organize 
them into boxes.

#### Features 

**Admin Features**
1. Admins can add new movies
2. Admins can manage new users

**Normal User Features**
1. User can view movies details (title, story line, release_date etc)
2. User can create multiple movie box
3. User can add movies to box 

**Guest User Features**
1. User can view movies details 
2. User can view other user movie boxes

#### Authentication

MovieBox api is protected using Api Keys. Each registered user (Admin or Normal User) have unique api keys, which is auto
generated from system on time of creation. Assigned Api Keys needs to be sent in HTTP authorization header for authentication.


#### Setup

**Clone This Project**

```
#!ruby

git clone git@bitbucket.org:bitf10m049/movie-box.git
```

**Bundle Install**


```
#!ruby

gem install bundle
bundle install
```


**Server Start**

```
#!ruby


rails server
```