Rails.application.routes.draw do

  namespace :api do
    namespace :v1, defaults:{format: 'json'} do
      resources :movies, except: [:new, :edit]
      resources :users, except: [:new, :edit] do
        resources :boxes, except: [:new, :edit] do
          member do
            post 'movies/:movie_id', to:'boxes#add_movie'
            get  :movies , to: 'boxes#show_movies'
          end
        end
      end
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # Serve websocket cable requests in-process
  # mount ActionCable.server => '/cable'
end
